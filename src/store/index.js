// store.js
import Vue from 'vue'
import Vuex from 'vuex'
import * as API from '../api'

Vue.use(Vuex)

export function createStore() {
    return new Vuex.Store({
        state: {
            items: 'hhh'
        },
        actions: {
            requestAsyncData({ commit, state }, data) {
                //commit('setItem', 'cb111a');
                return new Promise(function(resolve, reject) {
                    API.asynctest({
                        success: (response) => {
                            console.log(18, response);
                            commit('setItem', response)
                            resolve();
                        }
                    });
                });
            },
        },
        mutations: {
            setItem(state, data) {
                state.items = data;
            }
        }
    })
}